FROM php:7.4.1-fpm

RUN apt-get update &&  apt-get install -y \
    libmcrypt-dev \
    libcurl4-openssl-dev \
    libpng-dev \
    libpqtypes-dev \
    libzip-dev \
    unzip \
    git \
    && docker-php-ext-install curl gd pdo_pgsql pgsql zip


COPY composer.lock composer.json /var/www/
COPY database /var/www/database
WORKDIR /var/www

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer \
    && composer install  --optimize-autoloader --no-dev --no-scripts

COPY . /var/www
RUN chown -R www-data:www-data \
    /var/www/storage \
    /var/www/bootstrap/cache

RUN composer require crestapps/laravel-code-generator \
    && php artisan config:cache \
    && php artisan route:cache
