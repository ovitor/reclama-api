<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'author' => $faker->name(),
        'content' => $faker->sentence(),
        'published_at' => $faker->dateTime($max = 'now', $timezone = 'America/Fortaleza'),
    ];
});
